﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A derivation of <see cref="NetworkManager"/> interfacing the <see cref="MasterServerClient"/>.
/// Author: Gerard Ryan - July 2017
/// </summary>
[RequireComponent(typeof(MasterServerClient))]
public class MSCNetworkManager : NetworkManager {
    [Header("MSC Network Manager Configuration")]
    public bool isPublicServer = true;
    [Tooltip("The channel that the Server Message will be sent on to signify that this is not a connection for the MasterServerClient.")]
    [SerializeField] private byte serverChannelID = 0;
    [Tooltip("Defaults to MsgType.Ready")]
    public byte[] ServerMessage = CreateDefaultServerMessage();

    static MasterServerClient mSClient;
    public MasterServerClient MSClient { get { return (mSClient == null) ? mSClient = FindObjectOfType<MasterServerClient>() : mSClient; } }

    private static byte[] CreateDefaultServerMessage() {
        NetworkWriter writer = new NetworkWriter();
        writer.StartMessage(MsgType.Ready);
        writer.FinishMessage();
        return writer.ToArray();
    }

    /// <summary>
    /// Initialises the MasterServerClient class for a client to use.
    /// </summary>
    public void InitMasterServerClient() {
        MSClient.MaxConnections = maxConnections;
        MSClient.NetworkPort = MSClient.LANDiscoveryPort;
        MSClient.ChannelID = MSClient.ChannelID;
        if (customConfig) {
            connectionConfig.Channels.Clear();
            foreach (var c in channels) {
                connectionConfig.AddChannel(c);
            }
        }
        MSClient.ConnectionConfig = connectionConfig;
        MSClient.GlobalConfig = globalConfig;

        string errorDesc;
        if (!MSClient.Init(out errorDesc))
            Debug.LogError(errorDesc); 
    }

    /// <summary>
    /// Gets the successfully retrieved server listings
    /// </summary>
    /// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <param name="onError">(string) A description of the error that occurred.</param>
    public void GetServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null) {
        if (!MSClient.IsInitialised)
            InitMasterServerClient();
        MSClient.GetServerListings(newListing, onError);
    }

    /// <summary>
    /// Using <see cref="LANServerIDs"/> gets the successfully retrieved server listings.
    /// </summary>
    /// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <param name="onError">(string) A description of the error that occurred.</param>
    public void GetLANServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null) {
        if (!MSClient.IsInitialised)
            InitMasterServerClient();
        StartCoroutine(GetLANServerListings(MSClient.LANDiscoveryTimeout, newListing, onError));       
    }

    /// <summary>
    /// Retrieves a fresh list of LAN Server Listings.
    /// </summary>
    /// <param name="timeout">The time in milliseconds to try and retrieve listings for.</param>
    /// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <param name="onError">(string) A description of the error that occurred.</param>
    private IEnumerator GetLANServerListings(float timeout, Action<string, UInt16, byte[], int> newListing, Action<string> onError = null) {
        MSClient.LANServerIDs.Clear();
        yield return new WaitForSecondsRealtime(timeout / 1000); // milliseconds to seconds
        MSClient.GetLANServerListings(newListing, onError);
    }

    /// <summary>
    /// Connects to the given server with a MasterServerClient and creates the NetworkClient.
    /// </summary>
    /// <param name="address">The address of the MasterServerClient server.</param>
    /// <param name="port">The port of the MasterServerClient server.</param>
    /// <param name="onClientReturn">The newly created NetworkClient that is connected to the given address or null otherwise.</param>
    public void StartClient(string address, UInt16 port, Action<NetworkClient> onClientReturn) {
        if (client != null) {
            onClientReturn(null);
            Debug.LogWarning("Client not Started, already one created.");
            return;
        }
        if (!MSClient.IsInitialised)
            InitMasterServerClient();
        StartCoroutine(MSClient.ConnectToServer(address, port, serverChannelID, ServerMessage, (conID, errorDesc) => {
            if (conID != 0) {
                NetworkConnection netCon = new NetworkConnection();
                netCon.Initialize(address, MSClient.HostID, conID, new HostTopology(MSClient.ConnectionConfig, MSClient.MaxConnections));
                NetworkClient netClient = new NetworkClient(netCon);
                UseExternalClient(netClient);

                // Shut down the MSClient but not the network
                MSClient.Shutdown(out errorDesc, false);

                onClientReturn(netClient);
            } else {
                Debug.LogError(errorDesc);
                onClientReturn(null);
            }
        })); 
    }

    /// <summary>
    /// Calls <see cref="NetworkTransport.Shutdown()"/> after <see cref="NetworkManager.StopClient()"/>.
    /// </summary>
    new public void StopClient() {
        base.StopClient();
        NetworkTransport.Shutdown();
    }

    /// <summary>
    /// Extends the <see cref="NetworkManager"/>'s <see cref="MsgType.Connect"/> delegate with <see cref="MasterServerClient"/>'s <see cref="MsgType.Connect"/> functionality.
    /// </summary>
    /// <param name="conn"></param>
    override public void OnServerConnect(NetworkConnection conn) {
        NetworkMessage netMsg = new NetworkMessage();
        netMsg.conn = conn;
        MSClient.OnConnectDelegates(netMsg);
        base.OnServerConnect(conn);
    }

    /// <summary>
    /// Extends the <see cref="NetworkManager"/>'s <see cref="MsgType.Disconnect"/> delegate with <see cref="MasterServerClient"/>'s <see cref="MsgType.Disconnect"/> functionality.
    /// </summary>
    override public void OnServerDisconnect(NetworkConnection conn) {
        NetworkMessage netMsg = new NetworkMessage();
        netMsg.conn = conn;
        MSClient.OnDisconnectDelegates(netMsg);

        /// negates warning message because MSClient connections don't have players
        if (conn.playerControllers.Count > 0) {
            base.OnServerDisconnect(conn);
        }
    }

    /// <summary>
    /// Hooks in the master server client and publishes the server after the server is started.
    /// </summary>
    new public void StartServer() {
        StartServer(null, -1);
    }

    /// <summary>
    /// Hooks in the master server client and publishes the server after the server is started.
    /// </summary>
    new public void StartServer(ConnectionConfig config, int maxConnections) {
        base.StartServer(config, maxConnections);

        string errorDesc;
        if (!MSClient.Init(NetworkServer.serverHostId, MSClient.ChannelID, (UInt16)NetworkServer.listenPort, out errorDesc)) {
            Debug.LogError(errorDesc);
        } else {
            NetworkServer.RegisterHandler(MasterServerClient.MsgType.MasterServerData, MSClient.OnDataDelegates);
            StartCoroutine(MSClient.ServerStarted(isPublicServer, (success, outErrorDesc) => {
                if (success) {
                    Debug.Log(outErrorDesc);
                } else {
                    Debug.LogError(outErrorDesc);
                }
            }));
        }
    }

    /// <summary>
    /// calls <see cref="MasterServerClient.GracefullShutdown(Action{bool, string})"/> before <see cref="NetworkManager.StopServer()"/>
    /// </summary>
    new public void StopServer() {
        if (mSClient.IsInitialised) {
            StartCoroutine(mSClient.GracefulShutdown((success, errorDesc) => {
                if (!success)
                    Debug.LogError(errorDesc);
                base.StopServer();
            })); 
        }
    }
}
