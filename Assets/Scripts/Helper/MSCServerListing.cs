﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Aids in connecting and disconnecting a client when using <see cref="MSCNetworkManager"/>.
/// Author: Gerard Ryan - August 2017
/// </summary>
public class MSCServerListing : MonoBehaviour {
    [SerializeField] Button button;
    public string Address = "localhost";
    public UInt16 Port = 7777;

    [Header("Default Colours")]
    [SerializeField] ColorBlock defaultColours;

    [Header("Connected Colours")]
    [SerializeField] ColorBlock ConnectedColours;

    [Header("Connected Colours")]
    [SerializeField] ColorBlock ErrorColours;

    void Start() {
        button.onClick.AddListener(Connect);
        button.colors = defaultColours;
    }

    void Connect() {
        FindObjectOfType<MSCNetworkManager>().StartClient(Address, Port, (client) => {
            if (client != null) {
                button.colors = ConnectedColours;
                button.onClick.RemoveListener(Connect);
                button.onClick.AddListener(Disconnect);
            } else {
                button.colors = ErrorColours;
            }
        });
    }

    void Disconnect() {
        FindObjectOfType<MSCNetworkManager>().StopClient();
        button.colors = defaultColours;
        button.onClick.RemoveListener(Disconnect);
        button.onClick.AddListener(Connect);
    }
}
