﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interfaces the Master server and distributes/gathers rich game server information for presentation.
/// Author: Gerard Ryan - February 2017
/// </summary>
public class MasterServerClient : MonoBehaviour {


    /***************************************************** Types *****************************************************/
    public class MsgType : UnityEngine.Networking.MsgType {

        /// <summary>
        /// A message for the master server (client).
        /// </summary>
        public const short MasterServerData = 48;

        /// <summary>
        /// The highest value of the master server (client) message ids. User messages must be above this value.
        /// </summary>
        public const short MasterServerHighest = 48;

        new public static string MsgTypeToString(short value) {
            if (value <= Highest) {
                return UnityEngine.Networking.MsgType.MsgTypeToString(value);
            } else if (value == MasterServerData) {
                return "MasterServerData";
            } else {
                return string.Empty;
            }
        }
    }

    private enum DataMsgType : byte {
        /// <summary>
        /// Payload: | <see cref="MSG_OK"/> |
        /// </summary>
        MSG_OK = 0,

        /// <summary>
        /// Payload: | <see cref="MSG_ServerID"/> | <see cref="ServerID"/> serverIDs[] |
        /// </summary>
        MSG_ServerID = 1, // MSG_ServerID(s)

        /// <summary>
        /// Payload: | <see cref="MSG_NoServerIDs"/> |
        /// </summary>
        MSG_NoServerIDs = 2,

        /// <summary>
        /// Payload: | <see cref="MSG_ServerInfo"/> | <see cref="byte"/> serverInfo[<see cref="serverInfoLength"/>] | 
        /// </summary>
        MSG_ServerInfo = 3,

        /// <summary>
        /// Payload: | <see cref="CMD_SendServerID"/> |
        /// </summary>
        CMD_SendServerID = 4,

        /// <summary>
        /// Payload: | <see cref="CMD_GetServerIDs"/> |
        /// </summary>
        CMD_GetServerIDs = 5,

        /// <summary>
        /// Payload: | <see cref="CMD_RemoveServerID"/> |
        /// </summary>
        CMD_RemoveServerID = 6,

        /// <summary>
        /// Payload: | <see cref="CMD_GetInfo"/> |
        /// </summary>
        CMD_GetInfo = 7,

        /// <summary>
        /// Payload: | <see cref="CMD_NATPunchThrough"/> | <see cref="ServerID"/> dstServer |
        /// </summary>
        CMD_NATPunchThrough = 8
    }

    public struct ServerID {
        // +--------------------+-----------------+
        // | address (40 bytes) | port (2 bytes) |
        // +--------------------+-----------------+

        public static int AddressSize = 40;
        public static int Size = AddressSize + sizeof(UInt16);

        byte[] reccord;

        public string Address {
            get { return (reccord != null) ? System.Text.Encoding.ASCII.GetString(reccord, 0, AddressSize).Trim(new char[] { ' ', '\n', '\0', '\t' }) : null; }

            set {
                if (reccord == null || reccord.Length != Size)
                    reccord = new byte[Size];
                System.Text.Encoding.ASCII.GetBytes((value.Length < AddressSize) ? value.PadRight(AddressSize) : value.Substring(0, AddressSize)).CopyTo(reccord, 0);
            }
        }

        public UInt16 Port {
            get { return (reccord != null) ? BitConverter.ToUInt16(reccord, AddressSize) : (UInt16)0; }

            set {
                if (reccord == null || reccord.Length != Size)
                    reccord = new byte[Size];
                BitConverter.GetBytes(value).CopyTo(reccord, AddressSize);
            }
        }

        public ServerID(string address, UInt16 port) {
            reccord = new byte[Size];
            System.Text.Encoding.ASCII.GetBytes((address.Length < AddressSize) ? address.PadRight(AddressSize) : address.Substring(0, AddressSize)).CopyTo(reccord, 0);
            BitConverter.GetBytes(port).CopyTo(reccord, AddressSize);
        }

        public ServerID(byte[] serialized, int index = 0) {
            if (index == 0 && serialized.Length == Size) {
                reccord = serialized;
            } else {
                reccord = new byte[Size];
                Array.Copy(serialized, index, reccord, 0, Size);
            }
        }

        public byte[] Serialized {
            get { return reccord; }
        }

        public override string ToString() {
            return "{\"" + Address + "\", " + Port + "}";
        }

        public override int GetHashCode() {
            return (Address + Port).GetHashCode();
        }

        public override bool Equals(object obj) {
            return this.GetHashCode() == ((ServerID)obj).GetHashCode();
        }
    }

    private class Semaphore {
        private uint value;
        public uint Value { get { return value; } }

        public Semaphore(uint inital) {
            value = inital;
        }

        /// <summary>
        /// Attempts to decrement the semaphore, returns weather the caller should wait or if it succeeded.
        /// </summary>
        /// <returns>Weather the caller needs to wait for this resource.</returns>
        public bool Wait() {
            if (value > 0) {
                value--;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Increments the semaphore.
        /// </summary>
        public void Signal() {
            value++;
        }
    }

    /// <summary>
    /// Uses the DNS to retrieve the IPv6 string of the given URL or null on error.
    /// </summary>
    /// <returns>The IPv6 of the given URL or null on error</returns>
    public static string GetIPv6(string url) {
        System.Net.IPAddress ip = System.Net.Dns.GetHostAddresses(url)[0];
        switch (ip.AddressFamily) {
            case System.Net.Sockets.AddressFamily.InterNetwork:
                return "::ffff:" + ip.ToString();
            case System.Net.Sockets.AddressFamily.InterNetworkV6:
                return ip.ToString();
        }
        return null;
    }

    /******************************************* Master Server References ********************************************/
    [SerializeField] private string masterServerURL = "localhost";
    public string MasterServerURL { get { return masterServerURL; } }
    [SerializeField] private UInt16 masterServerPort = 1111;
    public int MasterServerPort { get { return masterServerPort; } }


    /**************************************** Master Server Client References ****************************************/
    /// <summary>
    /// Will not set if IsInitialised is true.
    /// </summary>
    public string NetworkAddress {
        get { return networkAddress; }
        set { if (!IsInitialised) networkAddress = value; }
    }
    [Header("Created Socket Configuration")]
    [SerializeField] private string networkAddress = null;

    /// <summary>
    /// Will not set if IsInitialised is true.
    /// </summary>
    public UInt16 NetworkPort {
        get { return networkPort; }
        set { if (!IsInitialised) networkPort = value; }
    }
    [SerializeField] private UInt16 networkPort = 7777;

    /// <summary>
    /// Will not set if IsInitialised is true.
    /// </summary>
    public GlobalConfig GlobalConfig {
        get { return (globalConfig == null) ? new GlobalConfig() : globalConfig; }
        set { if (!isInitialised) globalConfig = value; }
    }
    [SerializeField] private GlobalConfig globalConfig;

    /// <summary>
    /// Will not set if IsInitialised is true.
    /// </summary>
    public ConnectionConfig ConnectionConfig {
        get { return connectionConfig; }
        set { if (!IsInitialised) connectionConfig = value; }
    }
    [SerializeField] private ConnectionConfig connectionConfig;

    /// <summary>
    /// Will not set if IsInitialised is true.
    /// Should be the channel that the master server and client exclusively use to communicate.
    /// </summary>
    public byte ChannelID {
        get { return channelID; }
        set { if (!IsInitialised) channelID = value; }
    }
    [Tooltip("Should be the channel that the master server and client exclusively use to communicate.")]
    [SerializeField] private byte channelID = 2;

    /// <summary>
    /// Will not set if IsInitialised is true.
    /// </summary>
    public int MaxConnections {
        get { return maxConnections; }
        set { if (!isInitialised) maxConnections = value; }
    }
    [SerializeField] private int maxConnections = 512;

    [Tooltip("Needs to be >= sendServerIDBuffSize on the master server.")]
    [SerializeField] private ushort msgBuffSize = 1024;
    [Tooltip("Higher values may effect frame rates but allow for more simultaneous connections.")]
    [SerializeField] private uint maxNetEventsPerFrame = 25;
    public int HostID { get { return hostID; } }
    private int hostID = -1;
    private bool socketCreated = false;

    public UInt16 LANDiscoveryPort { get { return lANDiscoveryPort; } }
    [Header("LAN Discovery Configuration"), Tooltip("NetworkPort must equal LANDiscoveryPort for LAN servers to be discovered")]
    [SerializeField] private UInt16 lANDiscoveryPort = 7777;
    public int LANDiscoveryTimeout { get { return lANDiscoveryTimeout; } }
    [SerializeField] private int lANDiscoveryTimeout = 2000;
    [SerializeField] private int lANDiscoveryKey = 54461863;

    /// <summary>
    /// HasSet of unique LAN ServerID's populated by a <see cref="NetworkEventType.BroadcastEvent"/> event with the internal socket.
    /// <see cref="LANServerIDs.Clear()"/> may need to be called to make sure ServerIDs are current.
    /// </summary>
    public HashSet<ServerID> LANServerIDs { get { return (lANServerIDs == null) ? lANServerIDs = new HashSet<ServerID>() : lANServerIDs; } }
    private HashSet<ServerID> lANServerIDs;

    [Header("Subroutine Frequencies")]
    [SerializeField] private float idleConnectionCheck = 2f;
    [SerializeField] private float idleTimeDisconnect = 4f;

    private List<float> lastTimeConnActive;
    private int connectionCount;

    [Header("Misc"), Tooltip("Used by the client for getting server ID's and by the server and client for NAT punch through.")]
    [SerializeField] private uint maxOutgoingMSConnections = 2;
    private Semaphore outgoingMSConnections;
    private HashSet<int> outgoingMSConnectionIDs;
    [Tooltip("Used by the client for getting server listings and by the server and client for NAT punch through.")]
    [SerializeField] private uint maxOutgoingOtherConnections = 256;
    private Semaphore outgoingOtherConnections;
    private HashSet<int> outgoingOtherConnectionIDs;

    // Class Events
    private delegate void OnConnectEvent(int hostID, int conID);
    private OnConnectEvent onConnectDelegates;
    private delegate void OnDisconnectEvent(int hostID, int conID);
    private OnDisconnectEvent onDisconnectDelegates;
    private delegate void OnDataEvent(int hostID, int conID, int chanID, ref byte[] buff);
    private OnDataEvent onDataDelegates;


    /// <summary>
    /// <see cref="NetworkMessageDelegate"/> for a <see cref="UnityEngine.Networking.MsgType.Connect"/> handler when using this class with an external socket,
    /// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
    /// </summary>
    public NetworkMessageDelegate OnConnectDelegates {
        get {
            if (socketCreated)
                return null;
            return (NetworkMessage netMsg) => { onConnectDelegates(netMsg.conn.hostId, netMsg.conn.connectionId); };
        }
    }

    /// <summary>
    /// <see cref="NetworkMessageDelegate"/> for a <see cref="UnityEngine.Networking.MsgType.Disconnect"/> handler when using this class with an external socket,
    /// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
    /// </summary>
    public NetworkMessageDelegate OnDisconnectDelegates {
        get {
            if (socketCreated)
                return null;
            return (NetworkMessage netMsg) => { onDisconnectDelegates(netMsg.conn.hostId, netMsg.conn.connectionId); };
        }
    }

    /// <summary>
    /// <see cref="NetworkMessageDelegate"/> for a <see cref="MsgType.MasterServerData"/> handler when using this class with an external socket,
    /// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
    /// </summary>
    public NetworkMessageDelegate OnDataDelegates {
        get {
            if (socketCreated)
                return null;
            return (NetworkMessage netMsg) => {
                byte[] buff = netMsg.reader.ReadBytes(netMsg.reader.Length);
                onDataDelegates(netMsg.conn.hostId, netMsg.conn.connectionId, netMsg.channelId, ref buff);
            };
        }
    }

    /// <summary>
    /// An array of <see cref="byte[]"/> payloads for each of the <see cref="DataMsgType"/>s.
    /// </summary>
    byte[][] dataMsgCache;

    /// <summary>
    /// Initialises <see cref="dataMsgCache"/> with default messages so that they only need minor modification before sending.
    /// </summary>
    private void InitDataMsgCache() {
        byte[] dataMsgTypes = (byte[])Enum.GetValues(typeof(DataMsgType));
        dataMsgCache = new byte[dataMsgTypes.Length][];
        NetworkWriter writer = new NetworkWriter();

        foreach (byte msgType in dataMsgTypes) {
            writer.StartMessage(MsgType.MasterServerData);
            writer.Write(msgType);

            // add body if applicable 
            switch ((DataMsgType)msgType) {
                case DataMsgType.MSG_OK:
                case DataMsgType.MSG_ServerID:
                case DataMsgType.MSG_NoServerIDs:
                    break;

                case DataMsgType.MSG_ServerInfo:
                    writer.Write(ServerInfo, ServerInfoLength); /// updated by <see cref="ServerInfo"/>
                    break;

                case DataMsgType.CMD_SendServerID:
                case DataMsgType.CMD_GetServerIDs:
                case DataMsgType.CMD_RemoveServerID:
                case DataMsgType.CMD_GetInfo:
                    break;

                case DataMsgType.CMD_NATPunchThrough:
                    writer.Write(new byte[ServerID.Size], ServerID.Size); // The ServerID will have to be added on the fly
                    break;

                default:
#if UNITY_EDITOR || DEV_BUILD
                    Debug.LogWarning("No message buffer cached for " + (DataMsgType)msgType);
#endif
                    break;
            }

            writer.FinishMessage();
            dataMsgCache[msgType] = writer.ToArray();
        }
    }

    /// <summary>
    /// If this class has been initialised or not.
    /// </summary>
    public bool IsInitialised {
        get { return isInitialised; }
    }
    private bool isInitialised = false;

    /// <summary>
    /// Creates a socket for this class to use (using ConnectionConfig, MaxConnections and NetworkAddress:NetworkPort) and Initialises the class.
    /// Pre: Channel should be set to a channel in ConnectionConfig that it can have exclusive use of.
    /// </summary>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <returns>success or failure.</returns>
    public bool Init(out string errorDesc) {
        if (isInitialised) {
            errorDesc = "Master server client already initialised.";
            return false;
        }

        if (!SetupSocket(out errorDesc))
            return false;

        if (!InitialiseClass(out errorDesc))
            return false;

        isInitialised = true;
        return true;
    }

    /// <summary>
    /// Sets up the class to use the given socket and Initialises the class.
    /// </summary>
    /// <param name="hostID">The hostID/socket for this class to use.</param>
    /// <param name="channelID">The channel for this class to have exclusive use of.</param>
    /// <param name="port">The port that the given socket is bound to.</param>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <returns>success or failure.</returns>
    public bool Init(int hostID, byte channelID, UInt16 port, out string errorDesc) {
        if (isInitialised) {
            errorDesc = "Master server client already initialised.";
            return false;
        }

        if (!SetupSocket(hostID, channelID, port, out errorDesc))
            return false;

        if (!InitialiseClass(out errorDesc))
            return false;

        isInitialised = true;
        return true;
    }

    /// <summary>
    /// Using connectionConfig and maxConnections binds the socket networkAddress:networkPort for this class to use.
    /// Pre: channel should be set to a channel in connectionConfig that it can have exclusive use of.
    /// </summary>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <returns>success or failure.</returns>
    private bool SetupSocket(out string errorDesc) {
        if (isInitialised) {
            errorDesc = "Master server client already initialised.";
            return false;
        } else if (connectionConfig.ChannelCount <= channelID) {
            errorDesc = "channelID out of range, must be less than " + connectionConfig.ChannelCount;
            return false;
        }

        NetworkTransport.Init(GlobalConfig);

        if (string.IsNullOrEmpty(networkAddress)) {
            hostID = NetworkTransport.AddHost(new HostTopology(connectionConfig, maxConnections), networkPort);
        } else {
            hostID = NetworkTransport.AddHost(new HostTopology(connectionConfig, maxConnections), networkPort, networkAddress);
        }
        if (hostID < 0) {
            errorDesc = "Error binding to (" + ((string.IsNullOrEmpty(networkAddress)) ? "*" : networkAddress) + ":" + networkPort + ") may already be in use.";
            return false;
        }
        socketCreated = true;

        errorDesc = "";
        return true;
    }

    /// <summary>
    /// Sets the class to use the given socket and channel.
    /// Trusts that supplied socket is valid.
    /// </summary>
    /// <param name="hostID">The valid hostID/socket for this class to use.</param>
    /// <param name="channelID">The channel for this class to have exclusive use of.</param>
    /// <param name="port">The port that the given socket is bound to.</param>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <returns>success or failure.</returns>
    private bool SetupSocket(int hostID, byte channelID, UInt16 port, out string errorDesc) {
        if (isInitialised) {
            errorDesc = "Master server client already initialised.";
            return false;
        } if (hostID < 0) {
            errorDesc = "Invalid hostID";
            return false;
        }

        this.hostID = hostID;
        networkPort = port;
        this.channelID = channelID;

        errorDesc = "";
        return true;
    }

    /// <summary>
    /// Initialises class data structures, sets up LAN discovery and starts subroutines. 
    /// </summary>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <returns>success or failure.</returns>
    private bool InitialiseClass(out string errorDesc) {
        if (isInitialised) {
            errorDesc = "Master server client already initialised.";
            return false;
        } else if (hostID < 0) {
            errorDesc = "Invalid socket/hostID";
            return false;
        }

        updateBuff = new byte[msgBuffSize];
        lastTimeConnActive = new List<float>();
        connectionCount = 0;

        InitDataMsgCache();

        onDataDelegates += OnData;
        onConnectDelegates += OnConnect;
        onDisconnectDelegates += OnDisconnect;

        outgoingOtherConnections = new Semaphore(maxOutgoingOtherConnections);
        outgoingOtherConnectionIDs = new HashSet<int>();
        outgoingMSConnections = new Semaphore(maxOutgoingMSConnections);
        outgoingMSConnectionIDs = new HashSet<int>();

        byte error;
        NetworkTransport.SetBroadcastCredentials(hostID, lANDiscoveryKey, 0, 0, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            errorDesc = "LAN Discovery Failed (" + (NetworkError)error + ")";
        }

        if (idleConnectionCheck > 0)
            StartCoroutine(IdleDisconnect(idleConnectionCheck));

        errorDesc = "";
        return true;
    }

    /// <summary>
    /// Shuts down the class and utilised sockets so it may be reinitialised.
    /// Should be used as a last resort, for a graceful shut down <see cref="GracefulShutdown"/>
    /// </summary>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    /// <param name="ShutdownNetworkTransport">Leaves the NetworkTransport layer and sockets intact for external use.</param>
    /// <returns>success.</returns>
    public bool Shutdown(out string errorDesc, bool ShutdownNetworkTransport = true) {
        if (!isInitialised) {
            errorDesc = "Master server client isn't initialised.";
            return false;
        }

        errorDesc = "";

        if (serverRunning) {
            StartCoroutine(ServerStopped((success, error) => { }));
        }

        if (ShutdownNetworkTransport && socketCreated) {
            NetworkTransport.RemoveHost(hostID);
            NetworkTransport.Shutdown(); 
        }

        hostID = -1;
        StopAllCoroutines();
        lastTimeConnActive.Clear();
        lastTimeConnActive = null;

        onDataDelegates -= OnData;
        onConnectDelegates -= OnConnect;
        onDisconnectDelegates -= OnDisconnect;

        isInitialised = false;
        return true;
    }

    /// <summary>
    /// Stops the server and shuts down the class and utilised sockets so it may be reinitialised.
    /// </summary>
    /// <param name="onReturn">(bool) Weather server shut down gracefully, (string) A description of any error that occurred or information.</param>
    /// <param name="ShutdownNetworkTransport">Leaves the NetworkTransport layer and sockets intact for external use.</param>
    /// <returns></returns>
    public IEnumerator GracefulShutdown(Action<bool, string> onReturn, bool ShutdownNetworkTransport = true) {
        bool success = false;
        string errorDesc = "";
        yield return ServerStopped((outSuccess, outErrorDesc) => {
            if (Shutdown(out errorDesc, ShutdownNetworkTransport) && outSuccess) {
                success = true;
            } else {
                errorDesc = outErrorDesc;
            }
        });
        onReturn(success, errorDesc);
        yield break;
    }

    private void OnDestroy() {
        string unused;
        Shutdown(out unused);
    }

    private void OnApplicationQuit() {
        string unused;
        Shutdown(out unused);
    }


    /********************************************* Server References ********************************************/
    /// <summary>
    /// Info or data about your game server.
    /// </summary>
    public byte[] ServerInfo {
        get {
            if (serverInfo == null) {
                serverInfo = new byte[serverInfoLength];
            }
            return serverInfo;
        }
        set {
            if (value.Length == serverInfoLength) {
                serverInfo = value;
            } else {
                if (serverInfo == null)
                    serverInfo = new byte[serverInfoLength];
                if (value.Length < serverInfoLength) {
                    Array.Copy(value, serverInfo, value.Length);
                    Array.Clear(serverInfo, value.Length, serverInfoLength - value.Length);
                } else {
                    Array.Copy(value, serverInfo, serverInfoLength);
                    updateErrorDesc = "ServerInfo too long, Only " + serverInfoLength + " bytes will be used.";
                }
            }

            // update the ServerInfo message buffer
            if (dataMsgCache != null) {
                serverInfo.CopyTo(dataMsgCache[(byte)DataMsgType.MSG_ServerInfo], sizeof(ushort) + sizeof(short) + sizeof(DataMsgType)); 
            }
        }
    }
    private byte[] serverInfo;

    /// <summary>
    /// Size of ServerInfo.
    /// </summary>
    public int ServerInfoLength {
        get { return serverInfoLength; }
    }
    [SerializeField] private int serverInfoLength = 60;

    /// <summary>
    /// Returns if the server has been sent to the Master Server and the corresponding public ServerID if one exists.
    /// </summary>
    /// <param name="serverID">The public server ID or default(ServerID) if none exists.</param>
    /// <returns>ServerPublic</returns>
    public bool PublicServerID(out ServerID serverID) {
        if (!ServerPublic) {
            serverID = default(ServerID);
            return false;
        } else {
            serverID = publicServerID;
            return true;
        }
    }
    private ServerID publicServerID;

    /// <summary>
    /// If the server has been sent to the Master Server for everyone to see.
    /// </summary>
    public bool ServerPublic {
        get { return serverPublic; }
    }
    private bool serverPublic = false;

    /// <summary>
    /// Set to true by ServerStarted() and false by ServerStopped().
    /// </summary>
    public bool ServerRunning {
        get { return serverRunning; }
    }
    private bool serverRunning = false;

    /// <summary>
    /// Using ServerInfo and ServerPort sets up a server listing to be found and posted to the Master Server if ServerPublic is true.
    /// </summary>
    /// <param name="makePublic">Weather to make this server public or not.</param>
    /// <param name="onReturn">(bool) Weather server started successfully, (string) A description of any error that occurred or information.</param>
    public IEnumerator ServerStarted(bool makePublic, Action<bool, string> onReturn) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        if (serverRunning) {
            onReturn(false, "Server already running.");
            yield break;
        }

        if (!NetworkTransport.IsBroadcastDiscoveryRunning()) {
            byte error;
            if (!NetworkTransport.StartBroadcastDiscovery(hostID, lANDiscoveryPort, lANDiscoveryKey, 0, 0, null, 0, lANDiscoveryTimeout, out error) || (NetworkError)error != NetworkError.Ok) {
                onReturn(false, "LAN Discovery not started (" + (NetworkError)error + ")");
                yield break;
            } 
        }

        if (makePublic) {
            yield return PostServerID(0, (success, message) => {
                if (!success && NetworkTransport.IsBroadcastDiscoveryRunning())
                    NetworkTransport.StopBroadcastDiscovery();
                serverRunning = success;
                onReturn(success, message);
            });
        } else {
            serverRunning = true;
            onReturn(true, "Server Started");
        }
        yield break;
    }

    /// <summary>
    /// Using the given info and port sets up a server listing to be found and posted to the Master Server if ServerPublic is true.
    /// </summary>
    /// <param name="info">The information or data about the game server starting.</param>
    /// <param name="makePublic">Weather to make this server public or not.</param>
    /// <param name="onReturn">(bool) Weather server started successfully, (string) A description of any error that occurred or information.</param>
    public IEnumerator ServerStarted(byte[] info, bool makePublic, Action<bool, string> onReturn) {
        ServerInfo = info;
        yield return ServerStarted(makePublic, onReturn);
    }

    /// <summary>
    /// Stops the server listing from being found.
    /// </summary>
    /// <param name="onReturn">(bool) Weather server stopped successfully, (string) A description of any error that occurred.</param>
    public IEnumerator ServerStopped(Action<bool, string> onReturn) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        if (!serverRunning) {
            onReturn(false, "Server isn't running.");
            yield break;
        }

        if (NetworkTransport.IsBroadcastDiscoveryRunning())
            NetworkTransport.StopBroadcastDiscovery();

        serverRunning = false;

        if (serverPublic) {
            yield return RemoveServerID((success, errorDesc) => {
                onReturn(success, errorDesc);
            });
        } else {
            onReturn(true, "");
        }

        yield break;
    }


    /***************************************************** Update ****************************************************/
    /// <summary>
    /// Description of any errors or warnings that have occurred in this class.
    /// </summary>
    public string ErrorDescription {
        get { return updateErrorDesc; }
    }
    private string updateErrorDesc = "";

    // global so garbage collection doesn't work too hard unnecessarily
    // unused temp
    private UnityEngine.Networking.Types.NetworkID updateNetID;
    private UnityEngine.Networking.Types.NodeID updateNodeID;

    // Connection Info
    private int updateHostID;
    private int updateConID;
    private int updateChannelID;
    private string updateConIP;
    private int updateConPort;

    private int updateRecBuffSize;
    private byte[] updateBuff;
    private byte updateError;
    private uint updateNetEventCount;

    public void Update() {
        if (!isInitialised)
            return;

        updateNetEventCount = 0;
        while (socketCreated && updateNetEventCount < maxNetEventsPerFrame) {
            updateErrorDesc = "";

            NetworkEventType eType = NetworkTransport.Receive(out updateHostID, out updateConID, out updateChannelID, updateBuff, msgBuffSize, out updateRecBuffSize, out updateError);
            if ((NetworkError)updateError != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
                Debug.LogError("Receive: " + eType + " (" + (NetworkError)updateError + ")");
#endif
            }

            if (eType == NetworkEventType.Nothing)
                break;

            updateNetEventCount++;
            switch (eType) {

                // Error handle both
                case NetworkEventType.BroadcastEvent:
                case NetworkEventType.DataEvent:
                    if ((NetworkError)updateError != NetworkError.Ok) {
                        switch ((NetworkError)updateError) {
                            case NetworkError.WrongHost:
                                updateErrorDesc = "Socket (" + updateHostID + ") does not exist, please restart program.";
                                break;
                            case NetworkError.WrongConnection:
                                updateErrorDesc = "connection (" + updateConID + ") does not exist.";
                                break;
                            case NetworkError.WrongChannel:
                                updateErrorDesc = "channel (" + updateChannelID + ") does not exist.";
                                break;
                            case NetworkError.NoResources:
                                updateErrorDesc = "No resources to receive message.";
                                break;
                            case NetworkError.Timeout:
                                updateErrorDesc = "Timed out.";
                                break;
                            case NetworkError.MessageToLong:
                                updateErrorDesc = "Message too long.";
                                break;
                            case NetworkError.WrongOperation:
                                updateErrorDesc = "Operation Not supported.";
                                break;
                            case NetworkError.VersionMismatch:
                            case NetworkError.CRCMismatch:
                                updateErrorDesc = "Version Mismatch.";
                                if ((NetworkError)updateError == NetworkError.CRCMismatch)
                                    updateErrorDesc += " (Incorrect network configuration)";
                                break;
                        }

                        NetworkTransport.GetConnectionInfo(hostID, updateConID, out updateConIP, out updateConPort, out updateNetID, out updateNodeID, out updateError);
                        if ((NetworkError)updateError == NetworkError.Ok) {
                            updateErrorDesc = "Error with " + updateConIP + ":" + updateConPort + ", " + updateErrorDesc;
                        } else {
                            updateErrorDesc = "Error with ID " + updateConID + ", " + updateErrorDesc;
                        }
                        break;
                    }

                    // Data Event
                    if (eType == NetworkEventType.DataEvent) {
                        if (onDataDelegates != null) {
                            NetworkReader updateBuffReader = new NetworkReader(updateBuff);
                            ushort updateBuffSize = updateBuffReader.ReadUInt16();

                            short msgType = updateBuffReader.ReadInt16();
                            if (msgType != MsgType.MasterServerData) {
                                updateErrorDesc = "Unexpected MsgType received - " + MsgType.MsgTypeToString(msgType) + " (" + msgType + ")";
                                break;
                            }

                            byte[] MSDataBuff = updateBuffReader.ReadBytes(updateBuffSize);
                            onDataDelegates(updateHostID, updateConID, updateChannelID, ref MSDataBuff);
                        }
                    } else { // Broadcast Event
                        if (updateHostID == hostID) {
                            OnBroadcast(updateHostID);
                        }
                    }
                    break;

                case NetworkEventType.ConnectEvent:
                    if (onConnectDelegates != null)
                        onConnectDelegates(updateHostID, updateConID);
                    break;

                case NetworkEventType.DisconnectEvent:
                    if ((NetworkError)updateError != NetworkError.Ok) {
                        updateErrorDesc = "Server Unreachable: ";
                        if ((NetworkError)updateError == NetworkError.VersionMismatch || (NetworkError)updateError == NetworkError.CRCMismatch) {
                            updateErrorDesc += "Version Mismatch.";
                            if ((NetworkError)updateError == NetworkError.CRCMismatch)
                                updateErrorDesc += " (Incorrect network configuration)";
                        } else if ((NetworkError)updateError == NetworkError.Timeout) {
                            updateErrorDesc += "Connection timed out.";
                        } else {
                            updateErrorDesc += (NetworkError)updateError + ".";
                        }

                        NetworkTransport.GetConnectionInfo(hostID, updateConID, out updateConIP, out updateConPort, out updateNetID, out updateNodeID, out updateError);
                        if ((NetworkError)updateError == NetworkError.Ok) {
                            updateErrorDesc = "Error with " + updateConIP + ":" + updateConPort + ", " + updateErrorDesc;
                        } else {
                            updateErrorDesc = "Error with ID " + updateConID + ", " + updateErrorDesc;
                        }
                    }

                    if (onDisconnectDelegates != null)
                        onDisconnectDelegates(updateHostID, updateConID);
                    break;
            } 

        }
    }

    private void OnBroadcast(int hostID, Action<ServerID> newID = null) {
        if (!isInitialised) {
            updateErrorDesc = "MasterServerClient has not been initialised.";
            return;
        }

        if (NetworkPort != lANDiscoveryPort) {
            updateErrorDesc = "NetworkPort(" + NetworkPort + ") must equal LANDiscoveryPort(" + lANDiscoveryPort + ") to use LAN Discovery.";
            return;
        }

        if (hostID == this.hostID) {
            string address;
            int port;
            NetworkTransport.GetBroadcastConnectionInfo(hostID, out address, out port, out updateError);
            if ((NetworkError)updateError != NetworkError.Ok) {
                updateErrorDesc = "failed to get server info from LAN. (" + (NetworkError)updateError + ")";
                return;
            }

            ServerID id = new ServerID(address, (UInt16)port);
            if (LANServerIDs.Add(id) && newID != null)
                newID(id);
        }
    }

    private void OnData(int hostID, int conID, int channelID, ref byte[] buff) {
        if (!isInitialised) {
            updateErrorDesc = "MasterServerClient has not been initialised.";
            return;
        }

        if (hostID == this.hostID) {
            if (channelID == this.channelID) {

                // Update so last active time is accurate
                lastTimeConnActive[conID] = Time.realtimeSinceStartup;
            } else {

                // Ignore/Remove connection that wasn't for us
                lastTimeConnActive.Remove(conID);
            }
        }

        if (hostID == this.hostID && channelID == this.channelID) {
            byte error;
            NetworkReader data = new NetworkReader(buff);

            switch ((DataMsgType)data.ReadByte()) {
                case DataMsgType.CMD_NATPunchThrough:
                    if ((data.Length - data.Position) == ServerID.Size) {
                        UnityEngine.Networking.Types.NetworkID netID;
                        UnityEngine.Networking.Types.NodeID nodeID;

                        string address;
                        int port;

                        NetworkTransport.GetConnectionInfo(this.hostID, conID, out address, out port, out netID, out nodeID, out error);
                        if ((NetworkError)error == NetworkError.Ok) {
                            if (address == GetIPv6(masterServerURL) && port == masterServerPort) { // is master server
                                StartCoroutine(CompleteNATPunchThrough(conID, new ServerID(data.ReadBytes(ServerID.Size)), (outConID, outErrorDesc) => { updateErrorDesc = outErrorDesc; }));

                                NetworkTransport.Send(this.hostID, conID, this.channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out error);
                                if ((NetworkError)error != NetworkError.Ok) {
                                    updateErrorDesc = "Failed to respond that NAT punch through is being attempted. (" + (NetworkError)error + ")";
                                }
                            } else {
                                updateErrorDesc = "Non-MasterServer CMD_NATPunchThrough request";
                            }
                        } else {
                            updateErrorDesc = "failed to retrieve info of CMD_NATPunchThrough requester. (" + (NetworkError)error + ")";
                        }
                    } else {
                        updateErrorDesc = "Unexpected buffer size with CMD_NATPunchThrough";
                    }
                    break;

                case DataMsgType.CMD_SendServerID:
                    if ((data.Length - data.Position) == 0) {
                        if (serverPublic && serverRunning) {
                            StartCoroutine(PostServerID(conID, (success, outErrorDesc) => { if (!success) updateErrorDesc = outErrorDesc; }));
                        } else {
                            NetworkTransport.Send(this.hostID, conID, this.channelID, dataMsgCache[(byte)DataMsgType.MSG_NoServerIDs], dataMsgCache[(byte)DataMsgType.MSG_NoServerIDs].Length, out error);
                            if ((NetworkError)error != NetworkError.Ok)
                                updateErrorDesc = "Failed to respond that server isn't running/public. (" + (NetworkError)error + ")";
                        }
                    } else {
                        updateErrorDesc = "Unexpected buffer size with CMD_SendServerID";
                    }
                    break;

                case DataMsgType.CMD_GetInfo:
                    if ((data.Length - data.Position) == 0) {
                        if (serverRunning) {
                            StartCoroutine(PostServerInfoTo(conID, (success, outErrorDesc) => { updateErrorDesc = outErrorDesc; }));
                        } else {
                            string tmpStr;
                            DisconnectConnection(this.hostID, conID, out tmpStr);
                        }
                    } else {
                        updateErrorDesc = "Unexpected buffer size with CMD_GetInfo";
                    }
                    break;
            }
        }
    }

    private void OnConnect(int hostID, int conID) {
        if (!isInitialised) {
            updateErrorDesc = "MasterServerClient has not been initialised.";
            return;
        }

        if (hostID == this.hostID) {

            // if stealing an outgoing connections available, disconnect 
            if (connectionCount >= maxConnections - (outgoingMSConnections.Value + outgoingOtherConnections.Value)) {
                string errorDesc;
                if (DisconnectConnection(hostID, conID, out errorDesc))
                    updateErrorDesc = "Error Disconnecting from connection overflow " + errorDesc;
                return;
            } else {
                while (lastTimeConnActive.Count <= conID)
                    lastTimeConnActive.Add(-1f);
                lastTimeConnActive[conID] = Time.realtimeSinceStartup;
                connectionCount++;
            }
        }
    }

    private void OnDisconnect(int hostID, int conID) {
        if (!isInitialised) {
            updateErrorDesc = "MasterServerClient has not been initialised.";
            return;
        }

        if (hostID == this.hostID) {
            if (conID < lastTimeConnActive.Count) {
                lastTimeConnActive[conID] = -1f;
                connectionCount--; 
            }
            if (outgoingOtherConnectionIDs.Remove(conID))
                outgoingOtherConnections.Signal();
            if (outgoingMSConnectionIDs.Remove(conID))
                outgoingMSConnections.Signal();
        }
    }


    /******************************************** Connection Functionality *******************************************/
    /// <summary>
    /// Disconnects any idling connections
    /// </summary>
    /// <param name="repeatTimeout">if > 0 The time between checking again</param>
    /// <returns></returns>
    private IEnumerator IdleDisconnect(float repeatTimeout) {
        byte error;

        while (true) {
            if (connectionCount > 0) {
                float now = Time.realtimeSinceStartup;
                int actualConnectionCount = 0;

                for (int conID = 0; conID < lastTimeConnActive.Count; conID++) {
                    if (lastTimeConnActive[conID] < 0f) {
                        continue;
                    } else {
                        actualConnectionCount++;
                    }

                    if (now > lastTimeConnActive[conID] + idleTimeDisconnect) {
                        NetworkTransport.Disconnect(hostID, conID, out error);
                        if ((NetworkError)error != NetworkError.Ok) {

                            // call pseudo disconnect event
                            if (onDisconnectDelegates != null)
                                onDisconnectDelegates(this.hostID, conID);
#if UNITY_EDITOR || DEV_BUILD
                            Debug.LogError("Idle Disconnect error: " + (NetworkError)error);
#endif
                        }
                    }

                    // early exit check for widely varying connection count
                    if (actualConnectionCount == connectionCount) 
                        break;
                }
            }
            if (repeatTimeout > 0) {
                yield return new WaitForSecondsRealtime(repeatTimeout);
            } else {
                yield break;
            }
        }
    }

    /// <summary>
    /// Asynchronously connect to the standard Master Server socket with the socket for this class.
    /// </summary>
    /// <param name="onReturn">(int) Connection ID on success 0 otherwise. (string) A description of any error that occurred</param>
    private IEnumerator ConnectToMasterServer(Action<int, string> onReturn) {
        yield return ConnectTo(hostID, masterServerURL, masterServerPort, false, onReturn);
    }

    /// <summary>
    /// Uses MasterServerClient functionality to asynchronously connect to the given server 
    /// then sets up the connection for general server communication by sending serverMessage on serverChannel.
    /// </summary>
    /// <param name="address">The address to connect to.</param>
    /// <param name="port">The port to connect to.</param>
    /// <param name="serverChannel">The channel that the server uses other than ChannelID.</param>
    /// <param name="serverMessage">Data that the server may use to identify the new connection.</param>
    /// <param name="onReturn">(int) Connection ID on success 0 otherwise. (string) A description of any error that occurred</param>
    public IEnumerator ConnectToServer(string address, UInt16 port, byte serverChannel, byte[] serverMessage, Action<int, string> onReturn) {
        string errorDesc = "";
        int conID = 0;

        if (!isInitialised) {
            onReturn(conID, "MasterServerClient has not been initialised.");
            yield break;
        }
        if (serverChannel == channelID) {
            onReturn(conID, "Server channel cannot be ChannelID.");
            yield break;
        }
        if (address == masterServerURL && port == masterServerPort || GetIPv6(address) == GetIPv6(masterServerURL) && port == masterServerPort) {
            onReturn(conID, "Cannot have a general connection to the master server.");
            yield break;
        }

        yield return ConnectTo(hostID, address, port, true, (outConID, outErrorDesc) => { conID = outConID; errorDesc = outErrorDesc; });
        if (conID == 0) {
            onReturn(conID, errorDesc);
            yield break;
        }

        // set up for general connection (remove from monitored connections on server then free outgoing connection)
        byte error;
        NetworkTransport.Send(hostID, conID, serverChannel, serverMessage, serverMessage.Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(0, "Error sending the given message to the given server channel. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, conID, out errorDesc);
            yield break;
        }
        if (outgoingOtherConnectionIDs.Remove(conID))
            outgoingOtherConnections.Signal();

        onReturn(conID, "");
        yield break;
    }

    /// <summary>
    /// Asynchronously connect to the given server with the given socket.
    /// </summary>
    /// <param name="socketID">The socket to use for the connection.</param>
    /// <param name="address">The address to connect to.</param>
    /// <param name="port">The port to connect to.</param>
    /// <param name="onReturn">(int) Connection ID on success 0 otherwise. (string) A description of any error that occurred</param>
    private IEnumerator ConnectTo(int socketID, string address, UInt16 port, bool useNATPunchThrough, Action<int, string> onReturn) {
        byte error;

        // get outgoing connection slot
        if (address == masterServerURL && port == masterServerPort) {
            while (outgoingMSConnections.Wait()) {
                yield return null;
            }
        } else {
            while (outgoingOtherConnections.Wait()) {
                yield return null;
            }
        }

        int conID = NetworkTransport.Connect(socketID, GetIPv6(address), port, 0, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            if (address == masterServerURL && port == masterServerPort) {
                outgoingMSConnections.Signal();
            } else {
                outgoingOtherConnections.Signal();
            }

            onReturn(0, "Failed to allocate resources for connection. (" + (NetworkError)error + ")");
            yield break;
        }
        
        // add connection ID to taken outgoing connection slot
        if (address == masterServerURL && port == masterServerPort) {
            if (!outgoingMSConnectionIDs.Add(conID)) {
                outgoingMSConnections.Signal();
                onReturn(0, "Connection ID (" + conID + ") already using outgoing master server connection");
                yield break;
            }
        } else {
            if (!outgoingOtherConnectionIDs.Add(conID)) {
                outgoingOtherConnections.Signal();
                onReturn(0, "Connection ID (" + conID + ") already using outgoing other connection");
                yield break;
            }
        }

        bool connected = false;
        bool disconnected = false;
        OnConnectEvent TestConnected = (recSocID, recConID) => { connected = (recSocID == socketID && recConID == conID) ? true : connected; };
        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == socketID && recConID == conID) ? true : disconnected; };

        onDisconnectDelegates += TestDisconnected;
        onConnectDelegates += TestConnected;
        yield return new WaitUntil(() => { return connected || disconnected; });
        onConnectDelegates -= TestConnected;
        onDisconnectDelegates -= TestDisconnected;

        if (connected) {
            onReturn(conID, "");
            yield break;
        } else { // disconnected
            if (address == masterServerURL && port == masterServerPort) {
                onReturn(0, "Connection to the master server could not be established.");
            } else if (useNATPunchThrough) {
                yield return AttemptNATPunchThroughTo(new ServerID(address, port), onReturn);
            } else {
                onReturn(0, "Connection to " + address + ":" + port + " could not be established. (NAT punch through may help)");
            }
        }
    }

    /// <summary>
    /// Requests the master server to establish NAT punch through to the given server, then attempts to complete NAT punch through.
    /// </summary>
    /// <param name="dstServer">The destination server to NAT punch through to.</param>
    /// <param name="onReturn">(int) DstServer connection ID on success 0 otherwise. (string) A description of any error that occurred.</param>
    /// <returns></returns>
    private IEnumerator AttemptNATPunchThroughTo(ServerID dstServer, Action<int, string> onReturn) {
        int mSConID = 0;
        string errorDesc = "";

        yield return ConnectToMasterServer((conID, outErrorDesc) => { mSConID = conID; errorDesc = outErrorDesc; });
        if (mSConID == 0) {
            onReturn(0, "NAT punch through failed, " + errorDesc);
            yield break;
        }

        // send request
        byte error;
        dstServer.Serialized.CopyTo(dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough], sizeof(ushort) + sizeof(short) + sizeof(DataMsgType));
        NetworkTransport.Send(hostID, mSConID, channelID, dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough], dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            DisconnectConnection(hostID, mSConID, out errorDesc);
            onReturn(0, "Failed to send server ID for NAT punch through, " + errorDesc + " (" + (NetworkError)error + ")");
            yield break;
        }

        yield return CompleteNATPunchThrough(mSConID, dstServer, onReturn);
    }

    /// <summary>
    /// Completes the NAT punch through process after both endpoints receive the ok from the master server.
    /// </summary>
    /// <param name="mSConID">The connection ID for the master, will be disconnected on return.</param>
    /// <param name="dstServer">The server to NAT punch through to.</param>
    /// <param name="onReturn">(int) DstServer connection ID on success 0 otherwise. (string) A description of any error that occurred.</param>
    /// <returns></returns>
    private IEnumerator CompleteNATPunchThrough(int mSConID, ServerID dstServer, Action<int, string> onReturn) {
        string errorDesc = "";
        int destConID = 0;
        bool ok = false;
        bool disconnected = false;

        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == hostID && recConID == mSConID) ? true : disconnected; };
        OnDataEvent GetOK = (int recSocID, int recConID, int recChanID, ref byte[] buff) => {
            if (recSocID != hostID || recConID != mSConID || recChanID != channelID)
                return;

            if (buff.Length != sizeof(DataMsgType))
                return;

            NetworkReader data = new NetworkReader(buff);
            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_OK)
                return;
            
            ok = true;
        };

        // Get ok that NAT punch through is all set up for direct attempts
        onDisconnectDelegates += TestDisconnected;
        onDataDelegates += GetOK;
        yield return new WaitUntil(() => { return ok || disconnected; });
        onDataDelegates -= GetOK;
        onDisconnectDelegates -= TestDisconnected;

        if (disconnected) {
            onReturn(0, "Unexpected disconnection from master server while attempting NAT punch through to " + dstServer.Address + ":" + dstServer.Port + " " + errorDesc);
            yield break;
        }

        // TODO: port guessing for Restricted cone or symmetric NATs
        // attempt direct Connect
        yield return ConnectTo(hostID, dstServer.Address, dstServer.Port, false, (outConID, outErrorDesc) => { destConID = outConID; errorDesc = outErrorDesc; });
        onReturn(destConID, errorDesc);

        // clean up
        DisconnectConnection(hostID, mSConID, out errorDesc);
        yield break;
    }

    /// <summary>
    /// Disconnect from the given connection by the socket and it's connection ID. (Best Effort)
    /// </summary>
    /// <param name="socketID">The socket with the connection ID</param>
    /// <param name="conID">The connection ID for the connection to disconnect from.</param>
    /// <param name="errorDesc">A description of any error that occurred.</param>
    private bool DisconnectConnection(int socketID, int conID, out string errorDesc) {
        byte error;

        NetworkTransport.Disconnect(socketID, conID, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            errorDesc = "Failed to disconnect. (" + (NetworkError)error + ")";
            return false;
        }

        errorDesc = "";
        return true;
    }

    /// <summary>
    /// Waits for a DataMsgType.MSG_OK on the given connection then disconnects connection.
    /// </summary>
    /// <param name="conID">the connection to wait for the OK then disconnect</param>
    /// <param name="onReturn">(bool) weather an OK was received, (string) A description of any error that occurred.</param>
    /// <returns></returns>
    private IEnumerator ConfirmAndCloseConnection(int conID, Action<bool, string> onReturn) {
        string errorDesc = "";

        bool ok = false;
        OnDataEvent GetOK = (int recSocID, int recConID, int recChanID, ref byte[] buff) => {
            if (recSocID != hostID || recConID != conID || recChanID != channelID)
                return;

            if (buff.Length != sizeof(DataMsgType))
                return;

            NetworkReader data = new NetworkReader(buff);
            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_OK)
                return;

            ok = true;
        };

        bool disconnected = false;
        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == hostID && recConID == conID) ? true : disconnected; };

        onDisconnectDelegates += TestDisconnected;
        onDataDelegates += GetOK;
        yield return new WaitUntil(() => { return ok || disconnected; });
        onDataDelegates -= GetOK;
        onDisconnectDelegates -= TestDisconnected;

        // disconnect
        if (disconnected) {
            onReturn(false, "Unexpected Disconnection.");
            yield break;
        } else {
            DisconnectConnection(hostID, conID, out errorDesc);
        }

        onReturn(true, errorDesc);
        yield break;
    }


    /**************************************** Game Server Info Functionality *****************************************/
    /// <summary>
    /// Posts the server info set by ServerInfo & ServerPort to the given connection.
    /// </summary>
    /// <param name="conID">The active connection to send the server info to.</param>
    /// <param name="onReturn">(bool) if server info was posted successfully. (string) A description of any error that occurred</param>
    /// <returns></returns>
    private IEnumerator PostServerInfoTo(int conID, Action<bool, string> onReturn) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        string errorDesc = "";

        // compile and send info
        byte error;
        NetworkTransport.Send(hostID, conID, channelID, dataMsgCache[(byte)DataMsgType.MSG_ServerInfo], dataMsgCache[(byte)DataMsgType.MSG_ServerInfo].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Failed to send server info. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, conID, out errorDesc);
            yield break;
        }

        bool ok = false;
        yield return ConfirmAndCloseConnection(conID, (outOk, outErrorDesc) => { ok = outOk; errorDesc = outErrorDesc; });
        if (!ok) {
            onReturn(false, "Failed to post server info, " + errorDesc);
            yield break;
        }

        onReturn(true, errorDesc);
        yield break;
    }

    /// <summary>
    /// Retrieves the information about the server at the given socket.
    /// </summary>
    /// <param name="address">The server address to retrieve the info from.</param>
    /// <param name="port">The server port to retrieve the info from.</param>
    /// <param name="onReturn">(bool) weather the server info was retrieved successfully or not, (string) A description of any error that occurred.</param>
    /// <param name="result">(byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <returns></returns>
    public IEnumerator GetServerInfo(string address, UInt16 port, Action<bool, string> onReturn, Action<byte[], int> result) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        // if it is this server don't worry about a public connection
        if (ServerPublic && address == publicServerID.Address && port == publicServerID.Port) {
            address = "localhost";
            port = networkPort;
        }

        string errorDesc = "";
        int conID = 0;

        yield return ConnectTo(hostID, address, port, true, (outConID, outErrorDesc) => { conID = outConID; errorDesc = outErrorDesc; });
        if (conID == 0) {
            onReturn(false, errorDesc);
            yield break;
        }

        byte error;
        NetworkTransport.Send(hostID, conID, channelID, dataMsgCache[(byte)DataMsgType.CMD_GetInfo], dataMsgCache[(byte)DataMsgType.CMD_GetInfo].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Error sending CMD_GetInfo. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, conID, out errorDesc);
            yield break;
        }

        bool infoReceived = false;
        OnDataEvent GetInfo = (int recSocID, int recConID, int recChanID, ref byte[] buff) => {
            if (recSocID != hostID || recConID != conID || recChanID != channelID)
                return;

            if (buff.Length != sizeof(DataMsgType) + ServerInfoLength)
                return;

            NetworkReader data = new NetworkReader(buff);
            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_ServerInfo)
                return;

            int rtt = NetworkTransport.GetCurrentRTT(hostID, conID, out error);
            if ((NetworkError)error != NetworkError.Ok) {
                onReturn(false, "Failed to get round trip time. (" + (NetworkError)error + ")");
                DisconnectConnection(hostID, conID, out errorDesc);
                return;
            }

            result(data.ReadBytes(ServerInfoLength), rtt);
            infoReceived = true;
        };

        bool disconnected = false;
        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == hostID && recConID == conID) ? true : disconnected; };

        onDisconnectDelegates += TestDisconnected;
        onDataDelegates += GetInfo;
        yield return new WaitUntil(() => { return infoReceived || disconnected; });
        onDataDelegates -= GetInfo;
        onDisconnectDelegates -= TestDisconnected;

        if (disconnected) {
            onReturn(false, "Unexpected disconnection from " + address + ":" + port + ". ( a server may not be running )");
            yield break;
        }

        NetworkTransport.Send(hostID, conID, channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Failed to respond that info was retrieved successfully. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, conID, out errorDesc);
            yield break;
        }

        onReturn(true, errorDesc);
        yield break;
    }

    /// <summary>
    /// Gets the successfully retrieved server listings
    /// </summary>
    /// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <param name="onError">(string) A description of the error that occurred.</param>
    public void GetServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null) {
        StartCoroutine(GetServerIDs((idCount, errorDesc) => { if (idCount < 0) onError(errorDesc); }, (id) => {
            StartCoroutine(GetServerInfo(id.Address, id.Port, (success, errorDesc) => { if (!success) onError(errorDesc); }, (serverInfo, latency) => {
                newListing(id.Address, id.Port, serverInfo, latency);
            }));
        }));
    }

    /// <summary>
    /// Using <see cref="LANServerIDs"/> gets the successfully retrieved server listings.
    /// </summary>
    /// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
    /// <param name="onError">(string) A description of the error that occurred.</param>
    public void GetLANServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null) {
        if (NetworkPort != lANDiscoveryPort) {
            onError("NetworkPort(" + NetworkPort + ") must equal LANDiscoveryPort(" + lANDiscoveryPort + ") to use LAN Discovery.");
            return;
        }
        foreach (ServerID id in LANServerIDs) {
            StartCoroutine(GetServerInfo(id.Address, id.Port, (success, errorDesc) => { if (!success) onError(id + " " + errorDesc); }, (serverInfo, latency) => {
                newListing(id.Address, id.Port, serverInfo, latency);
            }));
        }
    }


    /************************************** Master Server ServerID Functionality *************************************/
    /// <summary>
    /// Posts the server ID to the master server. (attempts connection if MSConID is less than or equal to 0).
    /// </summary>
    /// <param name="mSConID">The connection ID for the connection to the master server or 0 to internally connect</param>
    /// <param name="onReturn">(bool) if server ID was posted successfully. (string) A description of any error that occurred or general message</param>
    /// <returns></returns>
    public IEnumerator PostServerID(int mSConID, Action<bool, string> onReturn) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        string errorDesc = "";

        if (mSConID <= 0) {
            yield return ConnectToMasterServer((conID, outErrorDesc) => { mSConID = conID; errorDesc = outErrorDesc; });
            if (mSConID == 0) {
                onReturn(false, "Failed to post server ID, " + errorDesc);
                yield break;
            }
        }

        byte error;
        NetworkTransport.Send(hostID, mSConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_ServerID], dataMsgCache[(byte)DataMsgType.MSG_ServerID].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            DisconnectConnection(hostID, mSConID, out errorDesc);
            onReturn(false, "Failed to send server ID. (" + (NetworkError)error + ")");
            yield break;
        }

        bool disconnected = false;
        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == hostID && recConID == mSConID) ? true : disconnected; };

        bool confirmationRecieved = false;
        OnDataEvent GetConfirmation = (int recSocID, int recConID, int recChanID, ref byte[] buff) => {
            if (recSocID != hostID || recConID != mSConID || recChanID != channelID)
                return;

            if (buff.Length != sizeof(DataMsgType) + ServerID.Size)
                return;

            NetworkReader data = new NetworkReader(buff);
            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_ServerID) {
                DisconnectConnection(hostID, mSConID, out errorDesc);
                return;
            }

            serverPublic = true;
            publicServerID = new ServerID(data.ReadBytes(ServerID.Size));
            confirmationRecieved = true;
        };

        onDisconnectDelegates += TestDisconnected;
        onDataDelegates += GetConfirmation;
        yield return new WaitUntil(() => { return confirmationRecieved || disconnected; });
        onDataDelegates -= GetConfirmation;
        onDisconnectDelegates -= TestDisconnected;

        if (DisconnectConnection(hostID, mSConID, out errorDesc))

        if (confirmationRecieved) {
            onReturn(true, "network port " + networkPort + " may need to be open to the internet. (port forwarding may be required)");
        } else {
            onReturn(false, "Failed to confirm that server was received");
        }
        yield break;
    }

    /// <summary>
    /// Removes any posted server ID. Server should be removed before closing the program.
    /// </summary>
    /// <param name="onReturn">(Bool) Weather the ServerID was successfully removed or not, (string) A description of any error that occurred.</param>
    /// <returns></returns>
    public IEnumerator RemoveServerID(Action<bool, string> onReturn) {
        if (!isInitialised) {
            onReturn(false, "MasterServerClient has not been initialised.");
            yield break;
        }

        int mSConID = 0;
        string errorDesc = "";

        yield return ConnectToMasterServer((x, y) => { mSConID = x; errorDesc = y; });
        if (mSConID == 0) {
            onReturn(false, "Failed to remove server ID, " + errorDesc);
            yield break;
        }

        byte error;
        NetworkTransport.Send(hostID, mSConID, channelID, dataMsgCache[(byte)DataMsgType.CMD_RemoveServerID], dataMsgCache[(byte)DataMsgType.CMD_RemoveServerID].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Failed to remove server ID. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, mSConID, out errorDesc);
            yield break;
        }

        serverPublic = false;

        bool ok = false;
        yield return ConfirmAndCloseConnection(mSConID, (outOk, outErrorDesc) => { ok = outOk; errorDesc = outErrorDesc; });
        if (!ok) {
            onReturn(false, "Failed to confirm removal of server ID, " + errorDesc);
            yield break;
        } else {
            onReturn(true, errorDesc);
            yield break;
        }
    }

    /// <summary>
    /// Gets a complete list of unique ServerIDs from the master server.
    /// </summary>
    /// <param name="onReturn">(int) The count of ServerID's retrieved and -1 on error, (string) A friendly description of any error that occurred</param>
    /// <param name="newServerID">(ServerID) A new ServerID that has successfully been retrieved.</param>
    /// <returns></returns>
    public IEnumerator GetServerIDs(Action<int, string> onReturn, Action<ServerID> newServerID) {
        if (!isInitialised) {
            onReturn(-1, "MasterServerClient has not been initialised.");
            yield break;
        }

        int mSConID = 0;
        string errorDesc = "";
        int serverIDCount = 0;

        // connect
        yield return ConnectToMasterServer((x, y) => { mSConID = x; errorDesc = y; });
        if (mSConID == 0) {
            onReturn(-1, "Failed to get ServerID, " + errorDesc);
            yield break;
        }

        byte error;
        NetworkTransport.Send(hostID, mSConID, channelID, dataMsgCache[(byte)DataMsgType.CMD_GetServerIDs], dataMsgCache[(byte)DataMsgType.CMD_GetServerIDs].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(-1, "Failed to get ServerID. (" + (NetworkError)error + ")");
            DisconnectConnection(hostID, mSConID, out errorDesc);
            yield break;
        }

        // Process ServerIDs
        bool dataProcessed = false;
        bool moreData = false;
        OnDataEvent ProccessServerServerIDs = (int recSocID, int recConID, int recChanID, ref byte[] buff) => {
            if (recSocID != hostID || recConID != mSConID || recChanID != channelID) {
                return;
            }

            if ((buff.Length - sizeof(DataMsgType)) % ServerID.Size > 0)
                return;

            NetworkReader data = new NetworkReader(buff);
            DataMsgType dataType = (DataMsgType)data.ReadByte();
            if (dataType == DataMsgType.MSG_ServerID) {
                moreData = true;
                while ((data.Length - data.Position) >= ServerID.Size) {
                    newServerID(new ServerID(data.ReadBytes(ServerID.Size)));
                    serverIDCount++;
                }
            } else if (dataType == DataMsgType.MSG_NoServerIDs) {
                moreData = false;
            }

            dataProcessed = true;
        };

        bool disconnected = false;
        OnDisconnectEvent TestDisconnected = (recSocID, recConID) => { disconnected = (recSocID == hostID && recConID == mSConID) ? true : disconnected; };

        onDisconnectDelegates += TestDisconnected;
        onDataDelegates += ProccessServerServerIDs;
        do {
            yield return new WaitUntil(() => { return dataProcessed || disconnected; });
            dataProcessed = false;

            if (disconnected) {
                break;
            } else if (moreData) { // OK the next lot to come
                NetworkTransport.Send(hostID, mSConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out error);
                if ((NetworkError)error != NetworkError.Ok) {
                    onDataDelegates -= ProccessServerServerIDs;
                    onDisconnectDelegates -= TestDisconnected;
                    onReturn(-1, "Failed to get all ServerIDs. (" + (NetworkError)error + ")");
                    DisconnectConnection(hostID, mSConID, out errorDesc);
                    yield break;
                }
            }
        } while (moreData);
        onDataDelegates -= ProccessServerServerIDs;
        onDisconnectDelegates -= TestDisconnected;

        if (disconnected) {
            onReturn(-1, "Unexpected disconnection retrieving ServerIDs.");
            yield break;
        }

        // disconnect
        DisconnectConnection(hostID, mSConID, out errorDesc);
        onReturn(serverIDCount, errorDesc);
        yield break;
    }
}
